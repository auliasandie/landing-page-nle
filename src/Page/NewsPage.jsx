import React, { Component } from 'react';
import axios from 'axios';
import { NavDropdown } from 'react-bootstrap';
import HtmlToReact from 'html-to-react';
import { Link } from "react-router-dom";
import { Link as Linkscroll } from "react-scroll";
import Moment from 'moment';
import './Home.css';
import './NewsPage.css';
import nle from '../assets/img/logo-nle.png'


class NewsPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            news: [],
            title: null,
            image: null,
            content: null,
            idnews: this.props.location.state.idnews,
            titlenews: this.props.location.state.titlenews,
            contentnews: this.props.location.state.contentnews,
            imagenews: this.props.location.state.imagenews,
            datenews: this.props.location.state.datenews,
            offset: 0,
            page: 5
        }
    }

    fetchingData() {
        axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
            .then(response => {
                const news = response.data.items;
                console.log(news.length)
                const slice = news.slice(this.state.offset, this.state.offset + this.state.page);
                const dataNews = slice.map((item,index )=> {
                    return (
                        <>
                            <div className="card.servicepage mb-3">
                                <div className="card-body card-page">
                                    <>
                                    <Link style={{textDecoration: 'none'}} to={{ pathname:`/nle/news/${index}`, state : {
                                        idnews : index,
                                        titlenews: item.title,
                                        contentnews: item.link,
                                        imagenews: item.thumbnail,
                                        datenews: Moment(item.pubDate).format('LLLL')
                                    }
                                    }}>
                                        <div className="row">
                                            <div className="col-md-6" style={{ padding: "0%"}}>
                                                <p  className="card-title" style={{ margin: "0%" }}>{item.title}</p>
                                                <p className="number">{Moment(item.pubDate).format('LLL')}</p>
                                            </div>
                                            <div className="col-md-6">
                                                <img className="img-fluid img-list-music" src={item.thumbnail} alt={index} />
                                            </div>
                                        </div>
                                    </Link>
                                    </>
                                </div>
                                <hr style={{margin:"0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)"}}/>
                            </div>
                        </>
                    )
                })
            this.setState({
                dataNews,
                news: response.data.items, 
                title: response.data.items.title, 
                image: response.data.items.thumbnail,
                content: response.data.items.content
            })
        })
        .catch(function (error) {
            console.log(error)
        })
    }

    componentDidMount() {
        this.fetchingData()
        window.scrollTo(0, 0);
    }

    render(){
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const news = this.props.location.state
        console.log(this.props)
        return (
            <>
                <div className="App">
                {/* <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '50%', float: 'left', marginLeft: '-429px' }}></img>
                                <img src={nle} style={{ width: '193%', marginLeft: '-691px', marginTop: '-4px' }}></img>
                            </Link>
                            
                            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                Menu
        <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav">
                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Linkscroll className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/about">About</a></Linkscroll>            </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="/nle/service"  style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Linkscroll>            </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Linkscroll>            </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Linkscroll>            </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Linkscroll>            </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="/nle/registration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/registration">Registration</a>
                                        </Linkscroll>            </li>
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/nle/FAQ" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/FAQ" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/registertraining" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/register-training" style={{ color: 'black', fontSize: '13px' }}>Register Training</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/tutorial" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/tutorial" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                        <button type="button" className="btn btn-outline-light" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '9px' }}>
                                            <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav> */}
                    <div className="row">
                        <div className="col-md-8" style={{ padding: "7% 6% 6% 6%" }}>
                                <h3 className="newspage-title" key="1" >{news.titlenews}</h3>
                            <img className="img-page" src={news.imagenews}/>
                            <div style={{ padding: "3% 5% 10% 5%" }}>
                                <p className="newspage-date">{Moment(news.datenews).format('LLLL')}</p>
                                <hr style={{margin: "0% 60% 2% 0%", borderTop: "1px solid rgba(0, 0, 0, 0.18)"}}/>
                                {/* <p style={{textAlign: 'justify'}}>{HtmlToReactParser.parse(news.contentnews)}</p> */}
                                <p style={{textAlign: 'justify'}}>
                                {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */}
                                {news.contentnews}
                                </p>
                            </div>
                        </div>
                        <div className="col-md-4" style={{ padding: "11% 9% 6% 1%"}}>
                            <>
                                <h5 className="latcard">LATEST NEWS</h5>
                                {this.state.dataNews}
                            </>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default NewsPage
