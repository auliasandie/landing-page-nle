import React, { Component } from 'react';
import { NavDropdown, Carousel, Button, Jumbotron } from 'react-bootstrap';
import axios from 'axios'
import HtmlToReact from 'html-to-react'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { Link } from "react-router-dom";


class Page extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            content: null,
            icon: null,
            title: null,
            // feed: []
        }
    }

    fetchingData() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=${this.props.match.params.code}`)
            .then(response => {
                console.log(response);
                this.setState({ data: response.data, content: response.data.content, title: response.data.title, icon: response.data.icon })
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    // getNews() {
    //     axios.get(`https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.inboundlogistics.com%2Fcms%2Frss%2Ftrucking%2F`)
    //         .then(response => {
    //             this.setState({ feed: response.data.items })

    //         })
    //         .catch(function (error) {
    //             console.log(error)
    //         })

    // }

    componentDidMount() {
        this.fetchingData()
        // this.getNews()
    }

    componentDidUpdate(prevProps){
        if (this.props.match.params.code !== prevProps.match.params.code) {
            this.fetchingData(this.props.match.params.code)
            
        }
    }

   

    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const item = this.state.data
        console.log(this.state.data)
        // console.log(HtmlToReactParser.parse(data.content))
        console.log(this.props)
        // console.log(this.state.feed)
       
        return (
            <>
          
                <section className="s-service" id="service"></section>

               <div className="App">
               <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '21%', float: 'left', marginLeft: '-429px' }}></img>
                                <img src={nle} style={{ width: '84%', marginLeft: '-162%', marginTop: '-4px', float: 'left' }}></img>
                            </Link>
                            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                Menu
        <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav" style={{marginLeft: '-8%'}}>                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/about">About</a></Link>           
                                    </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/service"  style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Link>            </li>
                                    
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/FAQ" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/FAQ" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                       
                                        <NavDropdown.Item href="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                    <Link to="/login" >
                                    <button type="button" className="btn btn-outline-light" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                        <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                

                    <h3 style={{ marginTop: '8%', paddingBottom: '3%' }}>{this.state.title}</h3>
                    <p style={{ width: '90%', textAlign: 'justify', marginLeft: '4%' }}>{HtmlToReactParser.parse(item.content)}</p>


             
               
                    <section>
                        <Jumbotron style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%',height: '350px'}}>
                            <div className="container" >
                         <img src={logo} style={{ width: "22%", marginTop: '-2%', marginLeft: '-91%' }}>
        </img> 
        <div className="address" style={{textAlign: 'left', marginLeft: '-6%', marginTop: '2%', fontSize: '14px'}}>
            <span>Jl. Ahmad Yani By Pass</span>
            <br></br>
            <span>Kantor Pusat Bea dan Cukai</span>
            <br></br>
            <span>Jakarta Timur</span>
        </div>
        <div className='row'>
            <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }}></img>
            <span style={{ marginTop: '-8%', marginLeft: '2%',fontSize: '13px' }}>nle@beacukai.go.id</span>
        </div>
        <div class="row">
            <img src={phone} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-3%', color: 'blue', fontSize: '13px' }}>
            </img>
            <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }}>021-1500225</span>

        </div>
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-9%', marginRight: '7%' }}></img>
                            </div>
                         </Jumbotron>
                        </section>
             </div>
            </>
        )

    }

}

export default Page;