import React, { Component } from 'react';
import { NavDropdown, Button, Jumbotron, Form, FormGroup } from 'react-bootstrap';
import axios from 'axios'
import HtmlToReact from 'html-to-react'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import { Link } from "react-router-dom";
import Faq from "react-faq-component";
import './Faq.css'
 
const data = {
    title: "NLE DOCS",
    rows: [
        {
            title: "National Ecosystem Logistics",
            content: `NLE adalah Ekosistem logistik yang menyelaraskan arus lalu lintas barang dan dokumen international sejak kedatangan sarana pengangkut hingga barang tiba di gudang, berorientasi pada kerja sama antar instansi pemerintah dan swasta, melalui pertukaran data, simplifikasi proses, penghapusan repetisi dan duplikasi, serta didukung oleh sistem teknologi informasi yang mencakup seluruh proses logistic terkait dan menghubungkan sistem – sistem logoistik yang telah ada.`,
        },
        {
            title: "Nunc maximus, magna at ultricies elementum",
            content:
                "Nunc maximus, magna at ultricies elementum, risus turpis vulputate quam, vitae convallis ex tortor sed dolor.",
        },
        {
            title: "Curabitur laoreet, mauris vel blandit fringilla",
            content: `Curabitur laoreet, mauris vel blandit fringilla, leo elit rhoncus nunc, ac sagittis leo elit vel lorem. 
            Fusce tempor lacus ut libero posuere viverra. Nunc velit dolor, tincidunt at varius vel, laoreet vel quam. 
            Sed dolor urna, lobortis in arcu auctor, tincidunt mattis ante. Vivamus venenatis ultricies nibh in volutpat. 
            Cras eu metus quis leo vestibulum feugiat nec sagittis lacus.Mauris vulputate arcu sed massa euismod dignissim. `,
        },
        {
            title: "What is the package version",
            content: "v1.0.0",
        },
    ],
};
 
const styles = {
    // bgColor: 'white',
    titleTextColor: "black",
    rowTitleColor: "black",
   
    // rowContentColor: 'grey',
    // arrowColor: "red",
};
 
export default class Tutorial extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    
    }
    render() {
        return (
            <div className="App">
               <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '21%', float: 'left', marginLeft: '-429px' }}></img>
                                <img src={nle} style={{ width: '84%', marginLeft: '-162%', marginTop: '-4px', float: 'left' }}></img>
                            </Link>
                            {/* <a className="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a> */}
                            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                Menu
        <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav" style={{marginLeft: '-8%'}}>                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Link className="c-main-about" to="/nle/about" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/about">About</a></Link>           
                                    </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/service"  style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/services">Services</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/news" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="/nle/news">News</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/apicollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/apicollaboration">API's</a>
                                        </Link>            </li>
                                    <li className="nav-item">
                                        <Link className="c-main-about" to="/nle/entitycollaboration" style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="/nle/entitycollaboration">Entity Collaboration</a>
                                        </Link>            </li>
                                    
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/FAQ" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/FAQ" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                    <Link to="/login" >
                                    <button type="button" className="btn btn-outline-light" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                        <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
            <div style={{marginTop: '10%', marginLeft:'-15%', color: 'black'}}>
                <Faq data={data} styles={styles} />
            </div>

            <section>
                        <Jumbotron fixed style={{ marginTop: '20%', width: '137%', marginLeft: '-18%',height: '350px'}}>
                            <div className="container" >
                         <img src={logo} style={{ width: "22%", marginTop: '-2%', marginLeft: '-91%' }}>
        </img> 
        <div className="address" style={{textAlign: 'left', marginLeft: '-6%', marginTop: '2%', fontSize: '14px'}}>
            <span>Jl. Ahmad Yani By Pass</span>
            <br></br>
            <span>Kantor Pusat Bea dan Cukai</span>
            <br></br>
            <span>Jakarta Timur</span>
        </div>
        <div className='row'>
            <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }}></img>
            <span style={{ marginTop: '-8%', marginLeft: '2%',fontSize: '13px' }}>nle@beacukai.go.id</span>
        </div>
        <div class="row">
            <img src={phone} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-3%', color: 'blue', fontSize: '13px' }}>
            </img>
            <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }}>021-1500225</span>
            {/* <p style={{ marginLeft: '43%', marginTop: '-7%', fontSize: '13px', color: '#6c6c6d' }}>021-1234567</p> */}
        </div>
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-9%', marginRight: '7%' }}></img>
                            </div>
                         </Jumbotron>
                        </section>
                        </div>
                        
        );
    }
}