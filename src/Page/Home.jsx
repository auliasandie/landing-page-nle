import React, { Component } from 'react';
import { NavDropdown, Carousel, Button, Jumbotron, Spinner, Card } from 'react-bootstrap';
import './Home.css'
import axios from 'axios';
import HtmlToReact from 'html-to-react';
import { Link as Linkscroll } from "react-scroll";
import { Link } from "react-router-dom";
import 'react-multi-carousel/lib/styles.css';
import trucking from '../assets/img/trucking.png'
import logoapi from '../assets/img/logo-api.png'
import mail from '../assets/img/email.png'
import phone from '../assets/img/phone.png'
import logo from '../assets/img/logonle.png'
import nle from '../assets/img/logo-nle.png'
import truck from '../assets/img/logoitruck.png'
import digico from '../assets/img/digico.png'
import mandiri from '../assets/img/mandiri.png'
import radar from '../assets/img/radar.png'
import prahu from '../assets/img/prahu.png'
import lontar from '../assets/img/lontar.png'
import logol from '../assets/img/logol.png'
import logistic from '../assets/img/logistic.png'
import clickargo from '../assets/img/clickargo.png'
import { StyleSheet, css } from 'aphrodite';
import FadeIn from 'react-fade-in';
import carousel1 from '../assets/img/first-carousel.jpeg'
import carousel2 from '../assets/img/second-carousel.jpeg'
import carousel3 from '../assets/img/third-carousel.jpeg'
import carousel4 from '../assets/img/four-carousel.jpeg'
import carousel5 from '../assets/img/five-carousel.jpeg'
import carousel6 from '../assets/img/six-carousel.jpeg'
import carousel7 from '../assets/img/seven-carousel.jpeg'
import carousel8 from '../assets/img/eight-carousel.jpeg'
import carousel9 from '../assets/img/nine-carousel.jpeg'
import * as Parser from 'rss-parser';
import { string } from 'prop-types';
import Moment from 'moment';
// import Fade from 'react-reveal/Fade';

let parser = new Parser();
class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            about: [],
            service: [],
            news: [],
            api: [],
            feed: [],
            collab: [],
            contentapi: null,
            titleapi: null,
            summaryapi: null,
            contentabout: null,
            titleabout: null,
            summaryabout: null,
            contentservice: null,
            titleservice: null,
            summaryservice: null,
            titlenews: null,
            contentnews: null,
            datenews: null,
            imagenews: null,
            idnews: null,
            iconservice: null,
            isLoading: false,
            iconcollab: null,
            titlecollab: null,
            iconapi: null


        }

    }

    fetchingAbout() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=about`)
            .then(response => {
                console.log(response);
                this.setState({ about: response.data, contentabout: response.data.content, titleabout: response.data.title, summaryabout: response.data.icon })
            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingService() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=service`)
            .then(response => {
                console.log(response);
                this.setState({ service: response.data.children, titleservice: response.data.title, summaryservice: response.data.children.summary, iconservice: response.data.children.icon, contentservice: response.data.children.content, isLoading: true })
            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingNews() {
        axios.get(`https://cors-anywhere.herokuapp.com/https://contohportal.herokuapp.com/getNews`)
            // .then(response => response.json())
            .then(response => {
                console.log(response);
                this.setState({ news: response.data })
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    fetchingApi() {
        axios.get(`https://portalapinle.herokuapp.com/getContent?code=apicollaboration`)
            .then(response => {
                console.log(response);
                this.setState({ api: response.data, contentapi: response.data.content, titleapi: response.data.title, iconapi: response.data.icon})
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    getNews() {
        axios.get(`https://cors-anywhere.herokuapp.com/https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.kontan.co.id%2Fnews%2Fexportexpert`)
            .then(response => {
                this.setState({ feed: response.data.items })

            })
            .catch(function (error) {
                console.log(error)
            })

    }

    fetchingCollab(){
        axios.get(`https://contohportal.herokuapp.com/getContent?code=entitycollaboration`)
        .then(response => {
            console.log(response);
            this.setState({ collab: response.data.category_code, iconcollab: response.data.category_code.icon, titlecollab: response.data.category_code.title })
        })
        .catch(function (error) {
            console.log(error)
        })

    }



    componentDidMount() {
        this.fetchingAbout()
        this.fetchingService()
        this.fetchingNews()
        this.fetchingApi()
        this.getNews()
        this.fetchingCollab()
    }

    render() {
        var HtmlToReactParser = HtmlToReact.Parser
        var HtmlToReactParser = new HtmlToReactParser()
        const about = this.state.about
        const api = this.state.api
        const item = this.state.service
        console.log(this.state.about)
        console.log(this.state.collab)
        console.log(this.props)

        return (
            <>
                <div className="App">
                    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                        <div className="container" style={{ marginLeft: '34%' }}>
                            <Link to="/">
                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: '21%', float: 'left', marginLeft: '-429px' }}></img>
                                <img src={nle} style={{ width: '84%', marginLeft: '-162%', marginTop: '-4px', float: 'left' }}></img>
                            </Link>
                            {/* <a className="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a> */}
                            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                Menu
        <i className="fa fa-bars"></i>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav" style={{marginLeft: '-8%'}}>
                                    <li className="nav-item" style={{ marginLeft: '-12%' }}>
                                        <Linkscroll className="c-main-about" to="about" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#about">About</a></Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="services" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#services">Services</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="news" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger-smooth" href="#news">News</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="api" smooth={true} duration={1000} offset={-50} style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#api">API's</a>
                                        </Linkscroll>
                                    </li>
                                    <li className="nav-item">
                                        <Linkscroll className="c-main-about" to="collaboration" smooth={true} duration={1000} offset={-50}ß style={{ color: 'white' }}>
                                            <a className="nav-link js-scroll-trigger" href="#collaboration">Entity Collaboration</a>
                                        </Linkscroll>
                                    </li>
                                    <NavDropdown title={<a className="text-light my-auto">Support</a>} id="nav-dropdown white">
                                        <NavDropdown.Item href="/FAQ" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/FAQ" style={{ color: 'black', fontSize: '13px' }}>FAQ</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/tutorialdocumentation" style={{ color: 'black', fontSize: '13px' }}>Tutorial Documentation</Link>
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>
                                            <Link to="/nle/contact" style={{ color: 'black', fontSize: '13px' }}>Contact Us</Link>
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <li>
                                        
                                        <Link to="/login" >
                                        <button type="button" className="btn btn-outline-light" style={{ width: '84px', height: '37px', marginLeft: '-6px', marginRight: '-46px', marginTop: '4px' }}>
                                        <p style={{ fontSize: '14px', lineHeight: '0.95', marginTop: '-1px' }}>Go To Platform</p>
                                        </button>
                                        </Link>
                                           
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <header className="masthead">


                        <Carousel style={{ marginTop: '4%' }} fade={true} indicators={false}>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel1}
                                    alt="First slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel2}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel3}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel4}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel5}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel6}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel7}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel8}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={carousel9}
                                    alt="Third slide"
                                    style={{ height: '550px' }}
                                />
                                <div class="container">
                                <div class="carousel-caption" style={{top: '44%'}}>
                                        <h1><mark style={{backgroundColor: 'grey', color: 'white'}}>National Logistics Ecosystem</mark></h1>
                                        <p className="subtitle" style={{marginTop: '25px'}}><mark style={{backgroundColor: '#233552', color: 'white'}}>Turning Problem Into Opportunity</mark></p>
                                        <a className="btn btn-light" href="/nle/about" style={{ color: 'black', textDecoration: "none" }}>
                                            <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    </div>
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </header>

                    <FadeIn transitionDuration={2000}>
                    <div style={{backgroundColor: '#233552'}}>
                    <section className="page-section" id="about">
                            <div className="container" style={{marginTop: '-7%'}} >

                                <div className="row">
                                    <div className="col-lg-12 text-center">
                                    {/* <Fade top> */}
                                    <h2 className="section-heading text-uppercase" style={{color: 'white'}}>{this.state.titleabout}</h2>
                                    {/* </Fade> */}
                                        {/* <h2 className="section-heading text-uppercase">{this.state.titleabout}</h2> */}
                                        {/* <h3 className="section-subheading text-muted">{HtmlToReactParser.parse(about.content)}</h3> */}
                                    </div>
                                </div>
                                <div className="row text-center" style={{marginTop: '-5%'}}>
                                    <p className="text-muted" style={{ width: '47%', textAlign: 'left', marginTop: '6%', color: 'white', }}>{HtmlToReactParser.parse(about.content)}</p>
                                    <img src={trucking} alt="industry" style={{ marginTop: '30%', width: '22%', marginLeft: '-47%' }}></img>
                                    <a className="btn btn-light" href="/nle/about" style={{ width: '13%', height: '10%', marginTop: '29%', marginLeft: '7%', color: 'black', border: '2px solid', textDecoration: "none" }}>
                                        <Link to="/nle/about" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>
                                    <div>
                                        {HtmlToReactParser.parse(about.icon)}
                                    </div>

                                </div>

                            </div>
                        </section>
                    </div>
                        
                        
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <div style={{backgroundColor: '#d0d4db'}}>
                        <section className="page-section" id="services">
                            <div className="container" style={{marginTop: '-7%'}}>
                                <div className="col-lg-12 text-center">

                                    <h2 className="section-heading text-uppercase">{this.state.titleservice}</h2>
                                    {/* <h3 className="section-subheading text-muted">{HtmlToReactParser.parse(about.content)}</h3> */}
                                </div>
                                <div className="row">
                                    {this.state.service.map((item, index) => {
                                        return (
                                            <>
                                                <div className="col-md-4">
                                                    <div className="services-grid">
                                                        <div className="service service1" style={{ marginTop: '0%', height: '349px' }}>
                                                            <i className="ti-bar-chart" />
                                                            <span className="fa-stack fa-4x">
                                                                {/* <i className="fa fa-circle fa-stack-2x text-primary"></i> */}
                                                                <img src={item.icon} style={{ width: '30px', height: '30px', marginTop: '-74%' }}></img>
                                                            </span>
                                                            <h4 className="title-service" style={{ marginTop: '-50%' }}>{item.title}</h4>
                                                            <p className="content-service" style={{ marginTop: '-24%' }}>{HtmlToReactParser.parse(item.content)}</p>
                                                        </div>

                                                    </div>
                                                </div>

                                            </>
                                        )
                                    })}
                                    <img src={require('../assets/img/ship.png')} alt="industry" style={{ width: '29%', height:'249px', float: 'right', marginTop: '49px', marginLeft: '26px' }}></img>

                                </div>


                            </div>

                        </section>
                        </div>
                        
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <section className="page-section" id="news">
                            <div className="container" style={{ marginTop: '-7%' }}>
                                <div className="row">
                                    <div className="col-lg-12 text-center">
                                        <h2 className="section-heading text-uppercase">NEWS</h2>
                                        {/* <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3> */}
                                    </div>
                                </div>
                                <div className="row" style={{ marginTop: '16%', marginLeft: '2%' }}>
                                    {this.state.feed.map((item, index) => {
                                        return (
                                            <>
                                                <div className="col-md-3">
                                                    <body className="body news">
                                                        <Link to={{
                                                            pathname: `/nle/news/${index}`, state: {
                                                                idnews: index,
                                                                titlenews: item.title,
                                                                contentnews: item.link,
                                                                imagenews: item.thumbnail,
                                                                datenews: item.pubDate
                                                            }
                                                        }} >
                                                            <article class="card news" style={{ height: '277px' }}>
                                                                <header class="cardThumb">
                                                                    <a href={`/nle/news/${index}`}>


                                                                        <img src={item.thumbnail} style={{ width: '225px' }}></img>
                                                                    </a>
                                                                </header>
                                                                <div class="cardBody">
                                                                    <h2 className="cardTitle"><a href>
                                                                        {/* <linkto to='/nle/news/:id'> </linkto> */}
                                                                        <h2 className="block-with-text" style={{ fontSize: '15px', textAlign: 'left' }}>{item.title}</h2></a></h2>
                                                                    {/* <p class="cardDescription">
                                                In ut quam quis magna pulvinar tempor at non elit. Ut lacinia elit vestibulum nulla accumsan, et fringilla mi accumsan. Etiam urna justo, luctus non porta eget, interdum quis tellus.
                                            </p> */}
                                                                </div>
                                                                <footer className="cardFooter">
                                                                    <span style={{ textAlign: 'center' }}>{Moment(item.pubDate).format('LLL')}</span>
                                                                </footer>
                                                            </article>
                                                        </Link>
                                                    </body>



                                                </div>

                                            </>
                                        )
                                    })}


                                </div>
                                <div>
                                    <a className="btn btn-light" href="/news" style={{ width: '13%', height: '10%', color: 'black', border: '2px solid', textDecoration: "none", marginTop: '18%' }}>
                                        <Link to="/news" style={{ textDecoration: 'none', color: 'black' }}>Read More</Link></a>

                                </div>
                            </div>
                        </section>
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <div style={{backgroundColor: '#c5cbd4'}}>
                        <section className="page-section" id="api">
                            <div className="container" style={{marginTop: '-7%'}}>
                                <div className="row" style={{marginTop: '-8%'}}>
                                    <div className="col-lg-12 text-center" >
                                        <h2 className="section-heading text-uppercase">{this.state.titleapi}</h2>
                                    </div>
                                </div>
                                <div className="row" style={{marginTop: '-8%'}}>
                                    <img src={this.state.iconapi} className="responsive" style={{ width: "200px", marginLeft: '4%', marginTop: '7%' }}></img>
                                    <p style={{ color: '#1d2d4b', width: '50%', marginLeft: '4%', marginTop: '11%', textAlign: 'justify' }}>
                                        {HtmlToReactParser.parse(api.content)}
                                    </p>
                                </div>
                                <Button variant="light" className="menu-button right" style={{
                                    width: '90px',
                                    height: '30px', marginLeft: '86%', marginTop: '-16%', borderRadius: '10px'
                                }}>
                                    <p style={{ fontSize: 12, textAlign: 'center' }}>Subscribe</p>
                                </Button>
                            </div>

                        </section>
                        </div>
                        
                    </FadeIn>

                    <FadeIn transitionDuration={2000}>
                        <section className="page-section" id="collaboration">
                            <div className="container" style={{marginTop: '-7%'}}>
                                <div className="row">
                                    <div className="col-lg-12 text-center">
                                        <h2 className="section-heading text-uppercase" style={{ marginBottom: '11%' }}>ENTITY COLLABORATION</h2>
                                    </div>
                                </div>
                                <div className="row row-1" style={{ marginTop: '-5%', marginLeft: '1%' }}>
                                   
                                             <div className="col-md-3 collab-menu collab-icon">
                                        <div className="collab">
                                            <span className="hidden-xs hidden-sm" style={{ marginLeft: '15%' }}>
                                                {this.state.titlecollab}
                                    <span className="pt-icon-standard pt-icon-chevron-down bt-caret"></span>
                                            </span>
                                            <div className="row row-2" style={{ marginLeft: '10px', marginTop: '22px' }}>
                                                <div className="row-md-3">
                                                <img src={require('../assets/img/kemenkeu.png')} style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Logo_of_the_Ministry_of_Transportation_of_the_Republic_of_Indonesia.svg/1200px-Logo_of_the_Ministry_of_Transportation_of_the_Republic_of_Indonesia.svg.png" style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src="https://upload.wikimedia.org/wikipedia/id/thumb/4/48/LOGO_KEMENTERIAN_KELAUTAN_DAN_PERIKANAN.png/1200px-LOGO_KEMENTERIAN_KELAUTAN_DAN_PERIKANAN.png" style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src="https://upload.wikimedia.org/wikipedia/commons/0/03/Logo_of_the_Ministry_of_Trade_of_the_Republic_of_Indonesia.jpg" style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src="https://www.pertanian.go.id/img/logo.png" style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src={require('../assets/img/kemenkes.jpg')} style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                <img src="https://www.pinterpandai.com/wp-content/uploads/2017/09/Logo-BPOM.jpg" style={{ width: '40px', height: '40px', marginLeft: '18px', marginBottom: '10px'}}></img>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                
                                    
                                   
                                    <div className="col-md-3 collab-menu">
                                        <span className="hidden-xs hidden-sm" style={{ marginLeft: '15%' }}>
                                            Private Sector
                                    <span className="pt-icon-standard pt-icon-chevron-down bt-caret"></span>
                                        </span>
                                        <div className="row row-2" style={{ marginLeft: '10px', marginTop: '22px'}}>
                                            <img src={truck} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={digico} style={{ width: '93px', height: '40px', marginLeft: '10px', marginBottom: '10px'}}></img>
                                            <img src={mandiri} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={radar} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={logol} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={clickargo} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={lontar} style={{ width: '99px', height: '30px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src={prahu} style={{ width: '99px', height: '30px',marginLeft: '10px', marginBottom: '10px' }}></img>

                                        </div>
                                    </div>
                                    <div className="col-md-3 collab-menu">
                                        <span className="hidden-xs hidden-sm" style={{ marginLeft: '15%' }}>
                                            Assosiation
                                    <span className="pt-icon-standard pt-icon-chevron-down bt-caret"></span>
                                        </span>
                                        <div className="row row-2" style={{ marginLeft: '10px', marginTop: '22px'}}>
                                        <div className="row-md-4">
                                            <img src="https://aptrindo.id/wp-content/uploads/2018/11/aptrindo_logo.png" style={{ width: '50px', height: '50px', marginLeft: '10px', marginBottom: '10px' }}></img>
                                            <img src="https://yt3.ggpht.com/a/AGF-l78wTSyViwyoeJFfLgrab8eUyJJMvvI9oZfP1g=s900-c-k-c0xffffffff-no-rj-mo" style={{ width: '50px', height: '56px', marginLeft: '10px', marginBottom: '14px', marginTop: '-6px'  }}></img>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcXang5YbCIrL5-RDVOd6CeJkof3OpSh2FxjMtmijewGcAweRp&usqp=CAU" style={{ width: '64px', height: '50px', marginTop: '-3px', marginBottom: '10px' }}></img>
                                            <img src={require('../assets/img/insa.jpg')} style={{ width: '68px', height: '65px', marginLeft: '11px', marginTop: '-11%', marginRight: '-13px' }}></img>
                                            <img src={require('../assets/img/ginsi.png')} style={{ width: '50px', height: '50px', marginLeft: '10px', marginTop: '-13%' }}></img>
                                            <img src={logistic} style={{ width: '57px', height: '62px', marginBottom: '10px', marginLeft: '10px', marginTop: '-9px' }}></img>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3 collab-menu">
                                        <div className="footer-call-center">
                                            <span className="hidden-xs hidden-sm" style={{ marginLeft: '15%' }}>
                                                Other Entity</span>
                                            <div className="row row-2" style={{ marginLeft: '10px', marginTop: '22px' }}>
                                               
                                                <img src="https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-thumbnail/s3/082012/untitled-1_86.png?itok=rkyigujJ" style={{ width: '57px', height: '54px', marginLeft: '61px' }}></img>
                                                <span style={{marginLeft: '17px', marginTop: '15px'}}>MAL</span>
                                                <img src={require('../assets/img/tpkoja.png')} style={{ width: '76px', height: '41px', marginLeft: '54px' }}></img>
                                                <span style={{marginLeft: '8px'}}>TLC</span>
                                                <img src={require('../assets/img/smpl.png')} style={{ width: '50px', height: '50px', marginLeft: '52px' }}></img>
                                                <span style={{marginLeft: '12px', marginTop: '18px'}}>GUDANG AEO</span>

                                                </div>

                                                

                                            </div>
                                            <div className="row row-2" style={{ marginLeft: '10px' }}>
                                                
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            {/* </div> */}

                        </section>
                    </FadeIn>


                    {/* <section> */}
                    <Jumbotron style={{ marginLeft: '-13%', width: '137%', marginLeft: '-18%', height: '350px' }}>
                        <div className="container" >
                            <img src={logo} style={{ width: "22%", marginTop: '-2%', marginLeft: '-91%' }}>
                            </img>
                            <div className="address" style={{ textAlign: 'left', marginLeft: '-6%', marginTop: '2%', fontSize: '14px' }}>
                                <span>Jl. Ahmad Yani By Pass</span>
                                <br></br>
                                <span>Kantor Pusat Bea dan Cukai</span>
                                <br></br>
                                <span>Jakarta Timur</span>
                            </div>
                            <div className='row'>
                                <img src={mail} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-8%', color: 'blue' }}></img>
                                <span style={{ marginTop: '-8%', marginLeft: '2%', fontSize: '13px' }}>nle@beacukai.go.id</span>
                            </div>
                            <div class="row">
                                <img src={phone} style={{ width: '29px', height: '25px', marginLeft: '37%', marginTop: '-3%', color: 'blue', fontSize: '13px' }}>
                                </img>
                                <span style={{ marginTop: '-3%', marginLeft: '2%', fontSize: '13px' }}>021-1500225</span>
                                {/* <p style={{ marginLeft: '43%', marginTop: '-7%', fontSize: '13px', color: '#6c6c6d' }}>021-1234567</p> */}
                            </div>
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/941px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png" style={{ width: "8%", float: 'right', marginTop: '-9%', marginRight: '7%' }}></img>
                        </div>
                    </Jumbotron>
                    {/* </section> */}



                </div>

            </>
        )
    }
}

const styles = {
  block: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    background: 'white',
    borderBottom: '1px solid rgba(255,255,255,.2)'
  },
  titlemotion: {
    textAlign: 'center',
    fontSize: '40px',
    color: '#fff',
    // fontFamily: 'Lato, sans-serif',
    fontFfamily: 'Poppins, sans-serif',
    fontWeight: 100
  },
};

export default HomePage;


