import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomePage from './Page/Home'
import NewsPage from './Page/NewsPage'
import NewsList from './Page/NewsList'
import Page from './Page/Page'
import LoginPage from './Page/Login'
import RegisterPage from './Page/Register'
import faq from './Page/Faq';
import Tutorial from './Page/Tutorial';


function App() {
  return (
    <div>
    
      <Router>
        <Route exact path='/' component={HomePage}/>
        <Route path='/nle/news/:index' component={NewsPage}/>
        <Route path='/news' component={NewsList}/>
        <Route path='/nle/:code' component={Page}/>
        <Route path='/login' component ={LoginPage}/>
        <Route path='/register' component={RegisterPage}/>
        <Route path='/faq' component={faq}/>
        <Route path='/tutorialdocumentation' component={Tutorial}/>
    </Router>
    </div>
  );
}

export default App;
